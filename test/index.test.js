/* eslint-disable no-console */
const serializeError = require('serialize-error');
const stableStringify = require('fast-json-stable-stringify');
const Logger = require('../index');

describe('Logger', () => {
    const invalidMessage = { key: 'value' };
    const validMessage = 'Test message.';
    const validObject = { key: 'value' };
    const validError = new TypeError('Some type error.');
    const formattedMessage = `${validMessage}\n${Logger.safeStringify(validObject)}`;
    const nonProductionEnvironment = 'staging';
    const productionEnvironment = 'production';
    const emptyMessage = '';
    const invalidObject = 10;

    beforeAll(() => {
        jest.spyOn(Logger, 'handleMalformedInput');
        jest.spyOn(Logger, 'formatMessage');
        jest.spyOn(Logger, 'isError');
        jest.spyOn(console, 'info').mockImplementation(jest.fn());
        jest.spyOn(console, 'debug').mockImplementation(jest.fn());
        jest.spyOn(console, 'warn').mockImplementation(jest.fn());
        jest.spyOn(console, 'error').mockImplementation(jest.fn());
    });

    afterAll(() => {
        jest.restoreAllMocks();
    });

    describe('safeStringify', () => {
        test('should test the supplied object for being error', () => {
            Logger.safeStringify(validObject);
            Logger.safeStringify(validError);
            expect(Logger.isError).toHaveBeenCalledTimes(2);
        });

        test('should serialize-error the object if object is an instance of Error', () => {
            expect.assertions(2);
            expect(
                JSON.parse(
                    Logger.safeStringify(validError),
                ),
            ).toEqual(
                JSON.parse(
                    JSON.stringify(
                        serializeError.serializeError(validError),
                    ),
                ),
            );
            /* eslint-disable jest/no-try-expect */
            try {
                // noinspection ExceptionCaughtLocallyJS
                throw validError;
            } catch (error) {
                expect(
                    JSON.parse(
                        Logger.safeStringify(error),
                    ),
                ).toEqual(
                    JSON.parse(
                        JSON.stringify(
                            serializeError.serializeError(error),
                        ),
                    ),
                );
            }
            /* eslint-enable jest/no-try-expect */
        });

        test('should replace circular reference with "__circular__" string', () => {
            const circularObject = { property: 'value' };
            circularObject.circularReference = circularObject;
            const circularObjectJSON = JSON.stringify({
                circularReference: '__cycle__',
                property: 'value',
            }, null, 2);

            const result = Logger.safeStringify(circularObject);
            expect(result).toBe(circularObjectJSON);
            expect(result).toBe(
                JSON.stringify(
                    JSON.parse(
                        stableStringify(circularObject, { cycles: true }),
                    ),
                    null,
                    2,
                ),
            );
        });

        test('should stringify JSON with 2 spaces and no replacer', () => {
            const jsonObject = {
                number: 2,
                object: {
                    array: [
                        1, 2, 'a',
                    ],
                    subProperty: 'value',
                },
            };

            expect(Logger.safeStringify(jsonObject)).toBe(
                JSON.stringify(jsonObject, null, 2),
            );
        });
    });

    describe('handleMalformedInput', () => {
        test('should print error to error output stream when called', () => {
            const stringifiedComposedMessage = Logger.safeStringify(
                { message: validMessage, object: invalidObject },
            );

            Logger.handleMalformedInput(validMessage, invalidObject);

            expect(console.error).toHaveBeenCalledTimes(1);
            expect(console.error.mock.calls[0][0]).toEqual(
                expect.stringContaining(stringifiedComposedMessage),
            );
        });
    });

    describe('formatMessage', () => {
        test('should return valid messages', () => {
            expect(Logger.formatMessage(validMessage)).toBe(validMessage);
        });

        test('should return valid message appended by valid stringified object', () => {
            expect(Logger.formatMessage(validMessage, validObject)).toBe(
                `${validMessage}\n${JSON.stringify(validObject, null, 2)}`,
            );
        });

        test('should NOT accept empty parameters', () => {
            expect(Logger.formatMessage()).toBeFalsy();
        });

        test('should NOT accept empty messages', () => {
            expect(Logger.formatMessage(emptyMessage)).toBeFalsy();
            expect(Logger.formatMessage(emptyMessage, validObject)).toBeFalsy();
            expect(Logger.formatMessage(emptyMessage, invalidObject)).toBeFalsy();
        });

        test('should NOT accept message types other than string', () => {
            const numberTypePositive = 20;
            const numberTypeNegative = -20;
            const numberTypeZero = 0;
            const booleanTypeTrue = true;
            const booleanTypeFalse = false;
            const symbolType = Symbol('symbol');
            const objectTypeNull = null;
            const objectTypeObject = { key: 'value' };
            const objectTypeArray = [1, 'value', 5, { key: 'value' }];
            const functionType = () => {
            };

            expect(Logger.formatMessage(numberTypePositive)).toBeFalsy();
            expect(Logger.formatMessage(numberTypeNegative)).toBeFalsy();
            expect(Logger.formatMessage(numberTypeZero)).toBeFalsy();
            expect(Logger.formatMessage(booleanTypeTrue)).toBeFalsy();
            expect(Logger.formatMessage(booleanTypeFalse)).toBeFalsy();
            expect(Logger.formatMessage(symbolType)).toBeFalsy();
            expect(Logger.formatMessage(objectTypeNull)).toBeFalsy();
            expect(Logger.formatMessage(objectTypeObject)).toBeFalsy();
            expect(Logger.formatMessage(objectTypeArray)).toBeFalsy();
            expect(Logger.formatMessage(functionType)).toBeFalsy();
        });

        test('should NOT accept object types other than object', () => {
            const stringType = 'String';
            const numberTypePositive = 20;
            const numberTypeNegative = -20;
            const numberTypeZero = 0;
            const booleanTypeTrue = true;
            const booleanTypeFalse = false;
            const symbolType = Symbol('symbol');
            const functionType = () => {
            };

            expect(Logger.formatMessage(stringType, stringType)).toBeFalsy();
            expect(Logger.formatMessage(stringType, numberTypePositive)).toBeFalsy();
            expect(Logger.formatMessage(stringType, numberTypeNegative)).toBeFalsy();
            expect(Logger.formatMessage(stringType, numberTypeZero)).toBeFalsy();
            expect(Logger.formatMessage(stringType, booleanTypeTrue)).toBeFalsy();
            expect(Logger.formatMessage(stringType, booleanTypeFalse)).toBeFalsy();
            expect(Logger.formatMessage(stringType, symbolType)).toBeFalsy();
            expect(Logger.formatMessage(stringType, functionType)).toBeFalsy();
        });

        test('should inform about malformed input when parameters are not supplied', () => {
            Logger.formatMessage();
            expect(Logger.handleMalformedInput).toHaveBeenCalledTimes(1);
        });

        test('should inform about malformed input when message is not of type string', () => {
            Logger.formatMessage(invalidMessage);
            expect(Logger.handleMalformedInput).toHaveBeenCalledTimes(1);
        });

        test('should inform about malformed input when message is an empty string', () => {
            Logger.formatMessage(emptyMessage);
            expect(Logger.handleMalformedInput).toHaveBeenCalledTimes(1);
        });

        test('should inform about malformed input when object is not of type object', () => {
            Logger.formatMessage(validMessage, invalidObject);
            expect(Logger.handleMalformedInput).toHaveBeenCalledTimes(1);
        });
    });

    describe('Logger Info', () => {
        test('should call format message with received arguments', () => {
            Logger.info(validMessage, validObject);
            expect(Logger.formatMessage).toHaveBeenCalledWith(
                validMessage,
                validObject,
            );
            expect(Logger.formatMessage).toHaveBeenCalledTimes(1);
        });

        test('should print to info stream for valid inputs', () => {
            Logger.info(validMessage, validObject);
            expect(console.info.mock.calls[0][0]).toBe(formattedMessage);
        });

        test('should NOT print to info stream for invalid inputs', () => {
            Logger.formatMessage.mockReturnValueOnce(false);
            Logger.info(validMessage, validObject);
            expect(Logger.formatMessage).toHaveBeenCalled();
            expect(console.info).not.toHaveBeenCalled();
        });
    });

    describe('Logger Debug', () => {
        beforeEach(() => {
            process.env.NODE_ENV = productionEnvironment;
        });

        afterEach(() => {
            delete process.env.NODE_ENV;
        });

        test('should call format message with received arguments', () => {
            process.env.NODE_ENV = nonProductionEnvironment;
            Logger.debug(validMessage, validObject);
            expect(Logger.formatMessage).toHaveBeenCalledWith(
                validMessage,
                validObject,
            );
            expect(Logger.formatMessage).toHaveBeenCalledTimes(1);
        });

        test('should print to debug stream for valid inputs', () => {
            process.env.NODE_ENV = nonProductionEnvironment;
            Logger.debug(validMessage, validObject);
            expect(console.debug.mock.calls[0][0]).toBe(formattedMessage);
        });

        test('should NOT print to debug stream for invalid inputs', () => {
            Logger.formatMessage.mockReturnValueOnce(false);
            process.env.NODE_ENV = nonProductionEnvironment;
            Logger.debug(validMessage, validObject);
            expect(Logger.formatMessage).toHaveBeenCalled();
            expect(console.debug).not.toHaveBeenCalled();
        });

        test('should NOT print anything to debug stream for production environment', () => {
            Logger.debug(validMessage, validObject);
            expect(console.debug).not.toHaveBeenCalled();
        });
    });

    describe('Logger Warn', () => {
        beforeEach(() => {
            process.env.NODE_ENV = productionEnvironment;
        });

        afterEach(() => {
            delete process.env.NODE_ENV;
        });

        test('should call format message with received arguments', () => {
            process.env.NODE_ENV = nonProductionEnvironment;
            Logger.warn(validMessage, validObject);
            expect(Logger.formatMessage).toHaveBeenCalledWith(
                validMessage,
                validObject,
            );
            expect(Logger.formatMessage).toHaveBeenCalledTimes(1);
        });

        test('should print to warn stream for valid inputs', () => {
            process.env.NODE_ENV = nonProductionEnvironment;
            Logger.warn(validMessage, validObject);
            expect(console.warn.mock.calls[0][0]).toBe(formattedMessage);
        });

        test('should NOT print to warn stream for invalid inputs', () => {
            Logger.formatMessage.mockReturnValueOnce(false);
            process.env.NODE_ENV = nonProductionEnvironment;
            Logger.warn(validMessage, validObject);
            expect(Logger.formatMessage).toHaveBeenCalled();
            expect(console.warn).not.toHaveBeenCalled();
        });

        test('should NOT print anything to warn stream for production environment', () => {
            Logger.warn(validMessage, validObject);
            expect(console.warn).not.toHaveBeenCalled();
        });
    });

    describe('Logger Error', () => {
        test('should call format message with received arguments', () => {
            Logger.error(validMessage, validObject);
            expect(Logger.formatMessage).toHaveBeenCalledWith(
                validMessage,
                validObject,
            );
            expect(Logger.formatMessage).toHaveBeenCalledTimes(1);
        });

        test('should print to error stream for valid inputs', () => {
            Logger.error(validMessage, validObject);
            expect(console.error.mock.calls[0][0]).toBe(formattedMessage);
        });

        test('should NOT print to error stream for invalid inputs', () => {
            Logger.formatMessage.mockReturnValueOnce(false);
            Logger.error(validMessage, validObject);
            expect(Logger.formatMessage).toHaveBeenCalled();
            expect(console.error).not.toHaveBeenCalled();
        });
    });
});
/* eslint-enable no-console */
