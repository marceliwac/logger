/* eslint-disable no-console */

const serializeError = require('serialize-error');
const stableStringify = require('fast-json-stable-stringify');

const productionEnvironments = ['production', 'prod'];

module.exports = class Logger {
    /**
     * Determines whether the object is of an Error type or its derivative (containing both
     * stack, message properties of type string).
     * @param object to be evaluated against being an Error.
     * @returns boolean true if object is an error, false otherwise.
     */
    static isError(object) {
        return !!(object && object.stack && object.message && typeof object.stack === 'string'
            && typeof object.message === 'string');
    }

    /**
     * Converts an object to a string and replaces the circular references to '__cycle__'.
     * The formatted JSON is a 2-space tabbed JSON.
     * @param object to be stringified.
     * @returns {string} stringified object.
     */
    static safeStringify(object) {
        let stableObject;
        if (Logger.isError(object)) {
            stableObject = serializeError.serializeError(object);
        } else {
            stableObject = JSON.parse(stableStringify(object, { cycles: true }));
        }

        return JSON.stringify(stableObject, null, 2);
    }

    /**
     * Processes the malformed input to the formatMessage method, when the object
     * parameter is not of object type.
     * @param message originally passed to formatMessage
     * @param object originally passed to formatMessage
     */
    static handleMalformedInput(message, object) {
        let formattedMessage = 'Logger method supplied with malformed input! Valid input requires '
            + 'non-empty (string) message with an optional (object) object. Supplied values: \n';
        formattedMessage += Logger.safeStringify({ message, object });
        console.error(formattedMessage);
    }

    /**
     * Formats the message based on contents of message and object, if the supplied values are not
     * string and object respectively, the returned value will not be a formatted message but a
     * signal to not print the message (false). Logging the objects with no message or an empty
     * string is not permitted.
     * @param message text to be printed.
     * @param object object to be logged alongside the message
     * @returns {string|boolean} formatted message if message parameter is correct, false otherwise
     */
    static formatMessage(message, object) {
        // Check if message was supplied
        if (typeof message !== 'undefined') {
            // Check if message is of valid type and non-empty
            if (typeof message === 'string' && message !== '') {
                // Check if object was supplied
                if (typeof object !== 'undefined') {
                    // Check if object is of valid type
                    if (typeof object === 'object') {
                        // Handle the cycles using stable stringify, then re-parse and stringify
                        // again using JSON to correct format (2 spaces per tab).
                        const json = Logger.safeStringify(object);
                        return `${message}\n${json}`;
                    }
                    Logger.handleMalformedInput(message, object);
                    return false;
                }
                return message;
            }
        }
        Logger.handleMalformedInput(message, object);
        return false;
    }

    /**
     * The most basic method used for logging the information. It will work across all environments
     * and should only display concise and necessary logs.
     * @param message text to be printed.
     * @param object object to be logged alongside the message
     */
    static info(message, object) {
        const formattedMessage = Logger.formatMessage(message, object);
        if (formattedMessage) {
            console.info(formattedMessage);
        }
    }

    /**
     * To be used for more verbose output that is useful only when the application is being
     * debugged. Any non-critical messages should be displayed using this function. Furthermore,
     * this function should not be printed on production environment to optimize the runtime
     * performance and reduce log clutter.
     * @param message text to be printed.
     * @param object object to be logged alongside the message
     */
    static debug(message, object) {
        // Ensure messages are not printed on production environment.
        if (!productionEnvironments.includes(process.env.NODE_ENV)) {
            const formattedMessage = Logger.formatMessage(message, object);
            if (formattedMessage) {
                console.debug(formattedMessage);
            }
        }
    }

    /**
     * To be used when a non-critical error occurs (i.e. the expected object is empty, but does not
     * break the application's logic). Furthermore, this function will not be printed on production
     * environment to optimize the runtime performance and reduce log clutter.
     * @param message text to be printed.
     * @param object object to be logged alongside the message
     */
    static warn(message, object) {
        // Ensure messages are not printed on production environment.
        if (!productionEnvironments.includes(process.env.NODE_ENV)) {
            1;
            const formattedMessage = Logger.formatMessage(message, object);
            if (formattedMessage) {
                console.warn(formattedMessage);
            }
        }
    }

    /**
     * An 'error' equivalent of the info() function that logs the information regardless of the
     * environment. It should only display the critical errors which cannot be recovered from.
     * @param message text to be printed.
     * @param object object to be logged alongside the message
     */
    static error(message, object) {
        const formattedMessage = Logger.formatMessage(message, object);
        if (formattedMessage) {
            console.error(formattedMessage);
        }
    }
};

/* eslint-enable no-console */
