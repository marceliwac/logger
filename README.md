# Logger

![Version](https://img.shields.io/npm/v/@marceliwac/logger?color=informational)
![License](https://img.shields.io/npm/l/@marceliwac/logger?color=informational)
![Downloads](https://img.shields.io/npm/dt/@marceliwac/logger)

Logger is a simple static class utility wrapper that can be used to substitute the default logging 
behaviour.

It provides methods for **info**, **debug**, **warn** and **error** outputs, supporting both 
`message` and `(message, object)` payloads, preventing logging of just the objects. This makes the
logs more legible and less cryptic.

## Installation
The package should be installed as dependency of the project.
```shell script
npm i --save @marceliwac/logger
```

## Usage
There are four streams of logs that can be accessed using this package: **info**, **debug**, 
**warn** and **error**. Only **info** and **error** logs will output when the environment is set to 
either `prod` or `production`.

To output the message, simply import the module and access its static methods. For logging objects,
use the same methods followed by an object payload. Supplied objects will be stringified and in the
cases where circular references are present - substituted with "`__cycle__`". The stringification
is provided by the 
[fast-json-stable-stringify](https://www.npmjs.com/package/fast-json-stable-stringify) package.

```javascript
const Logger = require('@marceliwac/logger');

Logger.info('This is a message.');
// "This is a message."

Logger.info('This is a message that has an object payload.', {key: 'value'});
// "This is a message that has an object payload."
// {
//   "key": "value"
// }
```

Separate methods handle the output to different streams.
```javascript
const Logger = require('@marceliwac/logger');

// Will print only when NODE_ENV is set to 'prod' or 'production'. 
Logger.info('This is an info message.');
Logger.error('This is an error message.');

// Will print regardless of the set environment. 
Logger.debug('This is an info message.');
Logger.warn('This is an error message.');
```

To enforce the clarity and structure within logs, in the cases when supplied message is not a string
or an empty string, the logging error message will be displayed. Errors will not be thrown to
prevent accidental application from breaking and supplied values will be logged regardless of
malformation. This is to allow the users to see why they made an error and enable debugging while 
still enforcing the log structure. Examples that would trigger this behaviour include:

```javascript
const Logger = require('@marceliwac/logger');

Logger.info();
// Logger method supplied with malformed input! Valid input requires non-empty (string) message with
//      an optional (object) object. Supplied values: 
// {}

Logger.debug({key: 'value'});
// Logger method supplied with malformed input! Valid input requires non-empty (string) message with
//      an optional (object) object. Supplied values: 
// {
//   "message": {
//     "key": "value"
//   }
// }

Logger.warn('');
// Logger method supplied with malformed input! Valid input requires non-empty (string) message with
//      an optional (object) object. Supplied values: 
// {
//   "message": ""
// }

Logger.error(null, {key: 'value'});
// Logger method supplied with malformed input! Valid input requires non-empty (string) message with
//      an optional (object) object. Supplied values: 
// {
//   "message": null,
//   "object": {
//     "key": "value"
//   }
// }
``` 

### Error Logging
In addition to the above, in the cases where the supplied object is of `Error` type (as checked by 
the `isError` method) the logger will not attempt to directly stringify it. Rather, the 
[serialize-error](https://www.npmjs.com/package/serialize-error) module will be used to serialize the
contents of the object.

## Future work
Several improvements could be made to the software. Primarily, the current version could be extended
to support both RequireJS and ES6-modules. Following is a non-exhaustive list of features planned 
for development:
- [ ] Add support for both RequireJS and ES6-module imports.
- [ ] Add CI/CD.


## Contributing
If you are interested in the further development of the module, feel free to submit an issue via 
[GitLab Issues](https://gitlab.com/marceliwac/logger/issues). It will be used both for bug tracking 
and feature development.
